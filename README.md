# Informer Attention Visualization
This module enables different kinds of visualization of the attentions inside the Informer time-series forecasting model presented in [Informer: Beyond Efficient Transformer for Long Sequence Time-Series Forecasting](https://arxiv.org/abs/2012.07436) by Zhou et al., available [here](https://github.com/zhouhaoyi/Informer2020).

## Installation
The folder envs/ contains the conda environment and additional pip requirements
to install and run this module.
```console
conda create --name informers --file informers_conda_env.txt 
```
Afterwards, activate the created conda environment and install the Python packages not installed via conda using  

```console
conda activate informers
pip install -r informers_python_requirements.txt  
```
This is a bit messy, and we will probably streamline this in the future.

## Usage Example


The simplest way to use this module is to add an instance of the `AttentionVisualizer` class as a member of the `Informer` class, pass the original data in 

### Modifiying model.py
Add the import

```python
from informer_attention_viz.attentionvisualizer import AttentionVisualizer
```
to the file model.py of the informer model and then add the instance as part of the `__init__()` function:

```python
...

self.pred_len = out_len
self.attn = attn
self.output_attention = output_attention

self.attention_visualizer = AttentionVisualizer(n_heads,
                                                    d_model,
                                                    'Timestep',
                                                    'Data',
                                                    discard_ratio=0.85)

...

```
To push data to the `AttentionVisualizer` instance, modify the `forward()` function of the `Informer` class in the following way:
```python
def forward(self, x_enc, x_mark_enc, x_dec, x_mark_dec, 
            enc_self_mask=None, dec_self_mask=None, dec_enc_mask=None,
            viz_data=None):

    enc_out = self.enc_embedding(x_enc, x_mark_enc)
    enc_out, attns = self.encoder(enc_out, attn_mask=enc_self_mask)

    dec_out = self.dec_embedding(x_dec, x_mark_dec)
    dec_out = self.decoder(dec_out, enc_out, x_mask=dec_self_mask, cross_mask=dec_enc_mask)

    dec_out = self.projection(dec_out)

    self.attention_visualizer.push(self.enc_embedding,
                                    viz_data, attns, x_enc)

    # dec_out = self.end_conv1(dec_out)
    # dec_out = self.end_conv2(dec_out.transpose(2,1)).transpose(1,2)

    if self.output_attention:
        return dec_out[:,-self.pred_len:,:], attns
    else:
        return dec_out[:,-self.pred_len:,:] # [B, L, D]
```
The new parameter `viz_data` serves to pass the original data to the `AttentionVisualizer` instance in order to enable the back projection onto the input data as part of the `render_projection()` visualization method. 

### Modifiying exp_informer.py

In the function `_process_one_batch()`, modify the call to the model in the following way:

```python
def _process_one_batch(self,
                        dataset_object,
                        batch_x,
                        batch_y,
                        batch_x_mark,
                        batch_y_mark,
                        viz_data):
...

if self.args.output_attention:
    outputs, attention = self.model(batch_x, batch_x_mark, dec_inp, batch_y_mark,
                                                                        viz_data=viz_data)

else:
    outputs = self.model(batch_x, batch_x_mark, dec_inp, batch_y_mark)
...

```
Modify `predict()` in the following manner:
```python
...

# If you want to render other data,
# for example the unscaled input data,
# switch out batch_x for the desired
# data source.
viz_data = batch_x

for i, (batch_x,batch_y,batch_x_mark,batch_y_mark) in enumerate(pred_loader):
            pred, true, _ = self._process_one_batch(
                pred_data, batch_x, batch_y, batch_x_mark, batch_y_mark, viz_data)
            preds.append(pred.detach().cpu().numpy())

...
```

### Modifiying main_informer.py
Add the calls to the desired visualization functions after the call to `exp.predict()`.
For example:
```python
...

if args.do_predict:
    print('>>>>>>>predicting : {}<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'.format(setting))
    exp.predict(setting, True)

    exp.model.attention_visualizer.render_projection('attention_projection.mp4',
                                                                    'Artist Name',
                                                                    fps=24,
                                                                    label_size=20,
                                                                    title_size=20,
                                                                    cmap='plasma')

    exp.model.attention_visualizer.render_combined('attention_rollout.mp4',
                                                                'Artist Name',
                                                                fps=24,
                                                                label_size=20,
                                                                title_size=20,
                                                                cmap='plasma')

    exp.model.attention_visualizer.render_individual_heads('attentions_individual.mp4',
                                                                            'Artist Name',
                                                                            fps=24,
                                                                            label_size=20,
                                                                            title_size=20,
                                                                            cmap='plasma')

...
```

This will consecutively render all currently implemented visualizations after the test set has been processed.

### Generating the Visualizations
Run the model using the flags `--do-predict --output_attention`.

## Visualization Types
This module currently implements three visualization types presented in the following.

### Individual Heads
This visualization simply displays the individual attention heads of each layer in a grid.

![SMD machine-1-1 Individual Heads Visualization Example](animations/machine-1-1_individual.mp4)

### Attention Rollout and "De-Embedding"
This visualization is built around the attention rollout proposed in [Quantifying Attention Flow in Transformers](https://arxiv.org/pdf/2005.00928) by Abnar and Zuidema as implemented by Jacob Gil, as seen [here](https://github.com/jacobgil/vit-explain/blob/main/vit_rollout.py).
In addition, it projects the attentions back into the shape of the input data using a transposed 1d convolution using an identity kernel.
This convolution has the same shape as the token convolution performed in the token embedding step that projects the data from the input shape c_in to d_model.

![SMD machine-1-1 Attention Rollout/"De-Embedding" Visualization Example](animations/machine-1-1_combined.mp4)

### Back-Projection onto Input Data
This visualization overlays the transposed 1d convolution back projection presented in the previous section onto the input data.

![SMD machine-1-1 Back-Projection Visualization Example](animations/machine-1-1_projection.mp4)

## Tested Environment
This module was tested using the following package/Python module versions:
| Package/Module | Version |
| ---            | ---     |
| python         | 3.9.12  |
| gstreamer      | 1.14.0  |
| ffmpeg         | 4.3     |
| pytorch        | 1.11.0  |
| numpy          | 1.21.2  |
| scipy          | 1.7.3   |
| scikit-learn   | 4.3     |
| matplotlib     | 3.5.1   |
| seaborn        | 0.11.2  |

Though other versions of these packages might work, they have not been tested, and I do not guarantee that the module can be built/run using any other versions of these packages/modules.


