# Copyright (c) 2020 Jacob Gildenblat
# Modifications copyright (C) 2023 CERN for the benefit of the ATLAS collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from .embed import DataEmbedding


# Based on the attention rollout presented in
# "Quantifying Attention Flow in Transformers" by Abnar and Zuidema
# as implemented by Jacob Gil in
# https://github.com/jacobgil/vit-explain/blob/main/vit_rollout.py

def rollout(attns, discard_ratio, head_fusion, device='cuda:0'):
    result = torch.eye(attns[0].size(-1),
                                requires_grad=False,
                                device=device)
    
    with torch.no_grad():
        for attention in attns:

            if head_fusion == "mean":
                attention_heads_fused = attention.mean(axis=1)
            elif head_fusion == "max":
                attention_heads_fused = attention.max(axis=1)[0]
            elif head_fusion == "min":
                attention_heads_fused = attention.min(axis=1)[0]
            else:
                raise "Attention head fusion type Not supported"

            flat = attention_heads_fused.view(attention_heads_fused.size(0), -1)

            _, indices = flat.topk(int(flat.size(-1)*discard_ratio), -1, False)
            indices = indices[indices != 0]
            flat[0, indices] = 0

            a = flat.view(*(attention_heads_fused.shape))

            I = torch.eye(attention_heads_fused.size(-1),
                                            device=device)

            a = (attention_heads_fused + I)/2
            # a = (attention_heads_fused + 0.001*I)

            # a = a/a.max(dim=-1)[0]
            a = a/a.sum(dim=-1)

            if a.size(-1) != attns[0].size(-1):

                a = a.unsqueeze(1)

                scale_factor = attns[0].size(-1)//\
                                                a.size(-1)

                # Check if power of two
                if not ((scale_factor & (scale_factor - 1) == 0)\
                                            and scale_factor != 0):
                    raise RuntimeError('Attention scale '\
                                        'factor not a power of 2')

                a = F.interpolate(a, attns[0].size(-1),
                                                mode='area')
                
                a = a.squeeze(1)

            result = torch.matmul(a, result)

    return result


def deembed(data_embedding: DataEmbedding,
                    attn,
                    use_token_conv_weights: bool = False):
    
    token_conv = data_embedding.value_embedding.tokenConv

    with torch.no_grad():

        if use_token_conv_weights:
            kernel = token_conv.weight

        else:
            kernel = torch.ones_like(token_conv.weight)/\
                                        token_conv.weight.shape[-1]

#             kernel = torch.zeros_like(token_conv.weight)
#             kernel[:, :, 1] = 1.0

        x = F.conv_transpose1d(attn,
                                kernel,
                                stride=token_conv.stride,
                                padding=token_conv.padding)
        
        return x.permute(0, 2, 1)


def get_deembedded_attns(data_embedding: DataEmbedding,
                                attns, x_enc,
                                n_heads, d_model,
                                discard_ratio: float = 0.85,
                                head_fusion: str = 'max'):
    
    attn_rollout = rollout(attns,
                            discard_ratio,
                            head_fusion)

    attn_reshaped = attn_rollout.unsqueeze(1)
    attn_reshaped = attn_reshaped.repeat_interleave(n_heads, dim=1)

    attn_reshaped = attn_reshaped.view(x_enc.shape[0],
                                            d_model,
                                            d_model//n_heads)

    attns_deembedded = deembed(data_embedding,
                                    attn_reshaped,
                                    use_token_conv_weights=False).cpu().numpy()
    
    attns = [np.squeeze(attn.detach().cpu().numpy()) for attn in attns]

    return [attns,
                attn_rollout.cpu().numpy(),
                attns_deembedded]


