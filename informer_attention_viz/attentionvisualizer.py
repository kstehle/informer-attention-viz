# Copyright (C) 2023 CERN for the benefit of the ATLAS collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from sklearn.preprocessing import MinMaxScaler

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import animation
import seaborn as sns
from tqdm import tqdm

from .embed import DataEmbedding
from .deembed import get_deembedded_attns

class AttentionVisualizer():
    """
    Parameters
    ----------
    n_heads : int
        Number of attention heads used in the informer.
    d_model : int
        Internal dimension d_model of the informer.
    x_label : str
        x-axis label used in the generated visualizations.
    y_label : str
        y-axis label used in the generated visualizations.
    discard_ratio : float, default=0.85
        Discard ratio used in the attention rollout.
        For a deeper explanation of this parameter, see
        https://github.com/jacobgil/vit-explain#filtering-the-lowest-attentions-in-every-layer
    head_fusion : str, default='max'
        Utilized attention rollout head fusion type.
        For a deeper explanation regarding this parameter, see
        https://github.com/jacobgil/vit-explain#different-attention-head-fusions
    writer_type : str, default='ffmpeg'
        matplotlib writer type used to render the visualizations.
    codec : str, default='mp4'
        Desired video codec of the generated visualizations.
    """

    def __init__(self,
                    n_heads: int,
                    d_model: int,
                    x_label: str,
                    y_label: str,
                    discard_ratio: float = 0.85,
                    head_fusion: str = 'max',
                    writer_type: str = 'ffmpeg',
                    codec: str = 'mpeg4') -> None:

        self._n_heads = n_heads
        self._d_model = d_model
        self._head_fusion = head_fusion
        self._discard_ratio = discard_ratio

        self._x_label = x_label
        self._y_label = y_label

        self._writer_type = writer_type
        self._codec = codec

        self._data_list = []
        self._attentions_list = []

    def push(self,
                data_embedding: DataEmbedding,
                data, 
                attns,
                x_enc) -> None:
        self._data_list.append(data)
        self._attentions_list.append(get_deembedded_attns(data_embedding,
                                                            attns, x_enc,
                                                            self._n_heads,
                                                            self._d_model,
                                                            self._discard_ratio,
                                                            self._head_fusion))


    def mask_by_magnitude_buckets(self,
                                    data,
                                    mask_data,
                                    bucket_count) -> list:

        magnitude_buckets =\
                np.linspace(0, 1, bucket_count + 1, endpoint=True)

        data_masked_by_magnitude_buckets = []

        for count in range(1, len(magnitude_buckets)):

            thresh_lower = magnitude_buckets[count - 1]
            # thresh_upper = magnitude_buckets[count]

            data_masked =\
                np.ma.masked_where(mask_data < thresh_lower, data)

            # data_masked =\
            #     np.ma.masked_where(mask_data > thresh_upper, data_masked)

            data_masked_by_magnitude_buckets.append(data_masked)

        return data_masked_by_magnitude_buckets


    def render_individual_heads(self,
                                    output_filename: str,
                                    artist_name: str,
                                    fps: int = 60,
                                    interval: int = 1,
                                    fig_size: tuple = (19.2, 10.8),
                                    label_size: float = 14,
                                    title_size: float = 20,
                                    cmap = 'viridis',
                                    fig_facecolor = 'darkgrey',
                                    ax_facecolor = 'lightgrey',
                                    seaborn_plotting_context: str = 'talk',
                                    writer_args: list = ['-qscale:v', '3']) -> None:
        """
        Parameters
        ----------
        output_filename : str
            Filename of the generated visualization.
        artist_name : str
            Artist name to use for the generated visualization.
        fps : int, default=60
            Frames per second of the generated visualization.
        interval : int, default=1
            Delay in milliseconds between frames.
        fig_size : tuple, default=(19.2, 10.8)
            Frame size of the generate visualization.
            In the current implementation, the parameters equal the
            pixel count in the respective dimension devided by 100.
        label_size : float, default=14
            Size of the axes labels in the generated visualization.
        title_size : float, default=20
            Title size.
        cmap : default='viridis'
            matplotlib colormap used to plot the attentions.
        fig_facecolor : default='darkgrey'
            Frame color of the generated plots.
        ax_facecolor : default='lightgrey'
            Background color of the individual subplots.
        seaborn_plotting_context : str, default='talk'
            seaborn plotting context.
        writer_args : list, default=['-qscale:v', '3']
            Additional parameters passed to the matplotlib animation writer.
            The default parameter is specific to the default H.264 codec
            and sets the video quality level.
        Returns
        -------
        None
        """

        layer_count = len(self._attentions_list[0][0])

        if not len(self._data_list):
            raise Warning('No data pushed to AttentionVisualizer.'
                                            'No output generated.')

        sns.set(rc={'figure.figsize': fig_size,
                        'figure.titlesize': title_size,
                        'axes.labelsize': label_size,
                        'axes.titlesize': title_size,
                        'figure.facecolor': fig_facecolor,
                        'axes.facecolor': ax_facecolor})

        pbar = tqdm(total=len(self._attentions_list),
                        desc='Rendering individual attention heads')

        fig = plt.figure()

        def animate(position):

            plt.clf()

            axes = fig.subplots(layer_count, self._n_heads)

            with sns.plotting_context(seaborn_plotting_context):

                for layer in range(layer_count):

                    ax = axes[layer, 0]

                    ax.text(-0.4, 0.5, f'Layer {layer + 1}',
                            size=label_size,
                            horizontalalignment='center',
                            verticalalignment='center',
                            rotation='horizontal',
                            transform=ax.transAxes)

                    for head in range(self._n_heads):
                        ax = axes[layer, head]

                        if layer == 0:
                            ax.text(0.5, 1.15, f'Head {head + 1}',
                                    size=label_size,
                                    horizontalalignment='center',
                                    verticalalignment='center',
                                    rotation='horizontal',
                                    transform=ax.transAxes)

                        data = self._attentions_list[position][0][layer][head, :, :]

                        shape_original = data.shape

                        data = np.atleast_2d(data.reshape(-1)).T
                        data = MinMaxScaler().fit_transform(data).T
                        data = data.reshape(*shape_original)

                        ax.tick_params(labelbottom=False, labelleft=False)

                        sns.heatmap(self._attentions_list[position][0][layer][head, :, :],
                                                                            ax=ax, cmap=cmap,
                                                                            square=True, cbar=False,
                                                                            vmin=0., vmax=1.)
                        
                plt.tight_layout()
                        
            pbar.update(1) 

        frames_to_render = np.arange(len(self._attentions_list))

        ani = animation.FuncAnimation(fig, animate,
                                        frames_to_render,
                                        interval=interval)

        Writer = animation.writers[self._writer_type]

        writer = Writer(fps=fps,
                            metadata=dict(artist=artist_name),
                            codec=self._codec,
                            extra_args=writer_args)

        ani.save(output_filename, writer)


    def render_combined(self,
                            output_filename: str,
                            artist_name: str,                   
                            fps: int = 60,
                            interval: int = 1,
                            fig_size: tuple = (19.2, 10.8),
                            label_size: float = 14,
                            title_size: float = 20,
                            cmap = 'viridis',
                            fig_facecolor = 'darkgrey',
                            ax_facecolor = 'lightgrey',
                            seaborn_plotting_context: str = 'talk',
                            writer_args: list = ['-qscale:v', '3']) -> None:
        """
        Parameters
        ----------
        output_filename : str
            Filename of the generated visualization.
        artist_name : str
            Artist name to use for the generated visualization.
        fps : int, default=60
            Frames per second of the generated visualization.
        interval : int, default=1
            Delay in milliseconds between frames.
        fig_size : tuple, default=(19.2, 10.8)
            Frame size of the generate visualization.
            In the current implementation, the parameters equal the
            pixel count in the respective dimension devided by 100.
        label_size : float, default=14
            Size of the axes labels in the generated visualization.
        title_size : float, default=20
            Title size.
        cmap : default='viridis'
            matplotlib colormap used to plot the attentions.
        fig_facecolor : default='darkgrey'
            Frame color of the generated plots.
        ax_facecolor : default='lightgrey'
            Background color of the individual subplots.
        seaborn_plotting_context : str, default='talk'
            seaborn plotting context.
        writer_args : list, default=['-qscale:v', '3']
            Additional parameters passed to the matplotlib animation writer.
            The default parameter is specific to the default H.264 codec
            and sets the video quality level.
        Returns
        -------
        None
        """
        
        if not len(self._data_list):
            raise Warning('No data pushed to AttentionVisualizer.'
                                            'No output generated.')

        sns.set(rc={'figure.figsize': fig_size,
                        'figure.titlesize': title_size,
                        'axes.labelsize': label_size,
                        'axes.titlesize': title_size,
                        'figure.facecolor': fig_facecolor,
                        'axes.facecolor': ax_facecolor})

        pbar = tqdm(total=len(self._attentions_list),
                        desc='Rendering attention rollout '
                                'and deconvoluted rollout')

        fig = plt.figure()

        def animate(position):

            plt.clf()

            axes = fig.subplots(1, 2)

            with sns.plotting_context(seaborn_plotting_context):

                ax = axes[0]
                
                data = self._attentions_list[position][1][0, :, :]

                shape_original = data.shape

                data = np.atleast_2d(data.reshape(-1)).T
                data = MinMaxScaler().fit_transform(data).T
                data = data.reshape(*shape_original)

                ax.tick_params(labelbottom=False, labelleft=False)
                ax.set_title('Rolled-Out Attentions')

                sns.heatmap(data,
                            ax=ax,
                            cmap=cmap,
                            square=True,
                            cbar=False,
                            vmin=0., vmax=1.)
            
                ax = axes[1]

                data = self._attentions_list[position][2][0, :, :]

                shape_original = data.shape

                data = np.atleast_2d(data.reshape(-1)).T
                data = MinMaxScaler().fit_transform(data).T
                data = data.reshape(*shape_original).T

                ax.tick_params(labelbottom=False, labelleft=False)
                ax.set_title('Back-Projected Attentions')

                sns.heatmap(data,
                                ax=ax,
                                cmap=cmap,
                                square=True,
                                cbar=False,
                                vmin=0., vmax=1.)
                
                plt.tight_layout()
                
            pbar.update(1)

        frames_to_render = np.arange(len(self._attentions_list))

        ani = animation.FuncAnimation(fig, animate,
                                        frames_to_render,
                                        interval=interval)

        Writer = animation.writers[self._writer_type]

        writer = Writer(fps=fps, metadata=dict(artist=artist_name),
                                                    codec=self._codec,
                                                    extra_args=writer_args)

        ani.save(output_filename, writer)


    def render_projection(self,
                            output_filename: str,
                            artist_name: str,
                            bucket_count: int = 8,                            
                            fps: int = 60,
                            interval: int = 1,
                            channels_lower: int = 0,
                            channels_upper: int = -1,
                            fixed_y_lower = None,
                            fixed_y_upper = None,
                            fig_size: tuple = (19.2, 10.8),
                            label_size: float = 14,
                            title_size: float = 20,
                            cmap = 'viridis',
                            fig_facecolor = 'darkgrey',
                            ax_facecolor = 'lightgrey',
                            seaborn_plotting_context: str = 'talk',
                            writer_args: list = ['-qscale:v', '3']) -> None:
        """
        Parameters
        ----------
        output_filename : str
            Filename of the generated visualization.
        artist_name : str
            Artist name to use for the generated visualization.
        bucket_count: int, default=8
            In the current implementation, the back-projected attentions
            are bucketed into a fixed number of buckets based on their
            magnitude. Afterwards, the data slices corresponding to
            individual attention buckets are rendered using the color
            corresponding to the respective attention magnitude.
            A larger number of buckets increases the granularity of the
            displayed attentions, but increases rendering time.
        fps : int, default=60
            Frames per second of the generated visualization.
        interval : int, default=1
            Delay in milliseconds between frames.
        channels_lower : int, default=0
            Lower bound of data channels to plot.
        channels_upper : int, default=-1
            Upper bound of data channels to plot.
        fixed_y_lower : default=None
            Fixed y-range lower value. If this is
            set to the default of None, a variable
            lower y will be used.
        fixed_y_upper : default=None
            Fixed y-range upper value. If this is
            set to the default of None, a variable
            upper y will be used.
        fig_size : tuple, default=(19.2, 10.8)
            Frame size of the generate visualization.
            In the current implementation, the parameters equal the
            pixel count in the respective dimension devided by 100.
        label_size : float, default=14
            Size of the axes labels in the generated visualization.
        title_size : float, default=20
            Title size.
        cmap : default='viridis'
            matplotlib colormap used to plot the attentions.
        fig_facecolor : default='darkgrey'
            Frame color of the generated plots.
        ax_facecolor : default='lightgrey'
            Background color of the individual subplots.
        seaborn_plotting_context : str, default='talk'
            seaborn plotting context.
        writer_args : list, default=['-qscale:v', '3']
            Additional parameters passed to the matplotlib animation writer.
            The default parameter is specific to the default H.264 codec
            and sets the video quality level.
        Returns
        -------
        None
        """
        
        if not len(self._data_list):
            raise Warning('No data pushed to AttentionVisualizer.'
                                                    'No output generated.')

        sns.set(rc={'figure.figsize': fig_size,
                    'figure.titlesize': title_size,
                        'axes.labelsize': label_size,
                        'axes.titlesize': title_size,
                        'figure.facecolor': fig_facecolor,
                        'axes.facecolor': ax_facecolor})

        pbar = tqdm(total=len(self._attentions_list),
                        desc='Rendering attention projection')

        fig = plt.figure()

        cmap = cm.get_cmap(cmap)

        def animate(position):

            start = position
            end = position + self._attentions_list[position][2].shape[1]

            window = self._data_list[position].squeeze()

            window = window[:, channels_lower:channels_upper]

            attn = self._attentions_list[position][2][0, :, channels_lower:channels_upper]

            shape_original = attn.shape

            attn = np.atleast_2d(attn.reshape(-1)).T
            attn = MinMaxScaler().fit_transform(attn).T
            attn = attn.reshape(*shape_original)

            plt.clf()

            ax = fig.subplots()

            with sns.plotting_context(seaborn_plotting_context):

                data_masked_all =\
                    self.mask_by_magnitude_buckets(window,
                                                        attn,
                                                        bucket_count)

                x = np.arange(start, end)

                alphas = np.linspace(0., 1., bucket_count, endpoint=True)

                if not isinstance(fixed_y_lower, type(None)):
                    ax.set_ylim(top=fixed_y_lower)

                if not isinstance(fixed_y_upper, type(None)):
                    ax.set_ylim(top=fixed_y_upper)
                    
                for data_masked, alpha in zip(data_masked_all, alphas):
                    ax.plot(x, data_masked,
                                    color=cmap(alpha),
                                    zorder=5)

                ax.set_title('Attentions Projected to Input Data')

                ax.set_xlabel(self._x_label)
                ax.set_ylabel(self._y_label)

                pbar.update(1)

        frames_to_render = np.arange(len(self._attentions_list))

        ani = animation.FuncAnimation(fig, animate,
                                        frames_to_render,
                                        interval=interval)

        Writer = animation.writers[self._writer_type]
        
        writer = Writer(fps=fps, metadata=dict(artist=artist_name),
                                codec=self._codec, extra_args=writer_args)

        ani.save(output_filename, writer)
